#!/usr/bin/sbcl --script

(setf *random-state* (make-random-state t))

(let ((fitnessfile (nth 1 sb-ext:*posix-argv*)))
	(if (or (null fitnessfile) (not (load fitnessfile)))
		(progn
			(format t "fitness file not found, run with path to lisp fitness file:~%~%    ./ga_runner.lisp fitnessN.lisp <other args...>~%~%")
			(sb-ext:quit))))

(defvar varcount (parse-integer (nth 2 sb-ext:*posix-argv*)))
(if (not (numberp varcount))
	(progn
		(format t "Fitness1 requires extra numerical arg: varcount (integer)~%~a~%" sb-ext:*posix-argv*)
		(sb-ext:quit)))

;; Pass command line args to the unique problem code
(ea-initialise-problem varcount)

;; the goal will differ for each problem
(defvar *goal* (ea-get-problem-goal))

;; global vars
(defvar *population* (generate-pop 100 *gene-count*))
(defvar *solution* (car *population*))
(defvar *mutation-rate* 30) ; in percent
(defvar *run-limit* 1000)

;; load in genetic operators
(load "../mutation.lisp")
(load "../selection.lisp")

(defun is-solution-found (population run-count run-limit)
	(loop for i from 0 below (length population) do
		(let ((test (nth i population)))
			(if (< (fitness test) (fitness *solution*))
					(setq *solution* test))))

	(if (or (equal run-count run-limit)
	        (< (fitness *solution*) 0.00001))
		t))

(defun ea-loop (population rl &optional (rc 0))
	(format t "run: ~a~a" rc #\Return)
	
	(setq population (selection population))
	(setq population (mutate-pop population))

	(if (is-solution-found population rl rc)
		(format t "~%Goal: ~a  Best result: ~a [~a:~a] at ~a runs.~%~%"
		        *goal*
		        (phenotype *solution*)
		        *solution*
		        (fitness *solution*)
		        rc)
		(ea-loop population rl (1+ rc))))


;; --- test
(ea-loop *population* *run-limit*)
