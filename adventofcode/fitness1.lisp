
;; read each line of the file and add it to a list as a number
(defun read-input-file (filename)
	(with-open-file (stream filename)
		(loop for line = (read-line stream nil)
		      while line
		      collect (parse-integer line))))

;; Read in list, order and return the list
(defun initialise-data (filename)
	(let ((data (read-input-file filename)))
		data))
(defvar *data* (initialise-data "input1"))

;;; ---------------------------------------------------------- ;;;

(defvar *gene-count* 0)
(defvar *split-count* 0)

(defun ea-initialise-problem (varcount)
	(setq *split-count* varcount)
	;; the length of a value in binary is
	;    [log_2(N)] + 1
	;; where [] is the floor function
	(setq *gene-count* (*
		(+ 1 (floor (log (length *data*) 2)))
		*split-count*)))

(defun ea-get-problem-goal () 2020)

;;; ---------------------------------------------------------- ;;;


(defun generate-pop (amount length)
	(loop for i from 0 below amount collect
		(generate-genotype length)))

(defun generate-genotype (length)
	(let ((genotype ""))
		(loop for i from 0 below length do
			(setq genotype (concatenate 'string genotype
				(string (nth (random 2) '(#\0 #\1))))))
		genotype))


(defun binary-to-index-list (genotype)
	(let* ((stepcount (/ *gene-count* *split-count*))
	       (steplist (loop for i from 0 below *split-count*
	                   collect (* i stepcount))))
		(loop for offset in steplist collect
			(loop for i from offset below (+ offset stepcount) sum
				(let ((power (+ (- stepcount 1 i) offset)))
					(* (expt 2 power) (if (equal (char genotype i) #\1) 1 0)))))))

(defun phenotype (genotype)
	(loop for index in (binary-to-index-list genotype) collect
		(nth (mod index (length *data*)) *data*)))

(defun fitness (genotype)
	(abs (- (reduce '+ (phenotype genotype)) 2020)))
