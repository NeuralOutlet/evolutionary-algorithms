
(defun mutate-pop (population)
	(loop for i from 0 below (length population) collect
		(arithmutate (nth i population))))

(defun mutate (genotype &optional (style t))
	(cond
		((equal style 'arithmutate) (arithmutate genotype))
		(t (format nil "~{~A~}" (flipbit genotype)))))

;; natural flipbit where each gene has a % chance to flip
;(defun flipbit (genotype)
;	(loop for i from 0 below (length genotype) do
;		(if (< (random 100) *mutation-rate*)
;			(let* ((bit (char genotype i)))
;				(if (equal bit #\0)
;					(setf (char genotype i) #\1)
;					(setf (char genotype i) #\0)))))
;	genotype)

;; one gene has a % chance to flip (used in 2013 code)
(defun flipbit (genotype)
	(let ((index (random (length genotype))))
		(loop for i from 0 below (length genotype) collect
			(if (and (equal i index) (< (random 100) *mutation-rate*))
				(if (equal (char genotype index) #\0)
					#\1
					#\0)
				(char genotype i)))))


;; The Arithmutation is arithmetic carry
;; applied after adding a random unit to the number.
;; This uses cl-sin functions to generate a random number
;; containing ony 1 unit then adding it to the genotype.

(defun arithmutate (genotype)
	(let* ((unit (gen-number "10" (length genotype) :unit #\1)))
		(sin-fixed+ genotype unit)))

