
;; currently just binary value
(defun phenotype (genotype)
	(let ((offset (- (/ *gene-count* 2) 1)))
		(loop for i from 0 below *gene-count* sum
			(* (expt 2 (- offset i))
			   (if (equal (char genotype i) #\1)
			    	1 0)))))

(defun fitness (genotype)
	(abs (- (phenotype genotype) *goal*)))

;; Rosenbrock Valley
(defun rosenbrock (x y &optional (heart 1))
	(+ (expt (- heart x) 2) (* 100 (expt (- y (* x x)) 2))))


