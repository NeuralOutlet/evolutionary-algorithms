(load "../cl-sin/sin/sin_library.lisp")

(setf *random-state* (make-random-state t))

;; to be replaced by SIN definition
;(defvar *sin-active-alphabet* (list #\0 #\1))

(defun generate-pop (amount length)
	(let* ((numsys (car *sin-active-systems*))
	       (alphabet (sin-alphabet numsys :digits-only t))
	       (digits (loop for pair in alphabet collect (car pair))))
		(loop for i from 0 below amount collect
			(generate-genotype length digits))))


(defun generate-genotype (length alphabet)
	(let ((genotype ""))
		(loop for i from 0 below length do
			(setq genotype (concatenate 'string genotype
				(string (nth (random (length alphabet)) alphabet)))))
		genotype))

;; activate the system to use
(load "../cl-sin/sin/sin_stl_systems.lisp")
(activate 'phinary)

;; global vars
(defvar *gene-count* 32)
(defvar *population* (generate-pop 10 *gene-count*))
(defvar *solution* (car *population*))
(defvar *mutation-rate* 30) ; in percent
(defvar *run-limit* 10000)
(defvar *goal* 8) ;2.3125)

;; load in genetic operators
(load "mutation.lisp") ;; includes loading in SIN
(load "selection.lisp")
(load "fitness.lisp")

(defun is-solution-found (population run-count run-limit)
	(loop for i from 0 below (length population) do
		(let ((test (nth i population)))
			(if (< (fitness test) (fitness *solution*))
					(setq *solution* test))))

	(if (or (equal run-count run-limit)
	        (< (fitness *solution*) 0.00001))
		t))

(defun ea-loop (population rl &optional (rc 0))
	(format t "run: ~a~a" rc #\Return)
	
	(setq population (selection population))
	(setq population (mutate-pop population))

	(if (is-solution-found population rl rc)
		(format t "~%Goal: ~a  Best result: ~a (~a) at ~a runs.~%~%~a~%"
		        *goal*
		        (float (phenotype *solution*))
		        *solution*
		        rc
		        population)
		(ea-loop population rl (1+ rc))))


;; --- test
(ea-loop *population* *run-limit*)
